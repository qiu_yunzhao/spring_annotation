package com.atguigu.dao;

import com.atguigu.utils.Animal;
import com.atguigu.utils.Cat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

@Repository
public class AnimalDao {

    @Autowired  // 根据类型装配
    private Cat cat;

    @Autowired
    public void setCat(Cat cat) {
        this.cat = cat;
    }

    @Autowired
    @Qualifier("dog") // 根据指定的id装配
    private Animal animal;

    @Autowired(required = false) // 容器中没有匹配的组件就不装配
    @Qualifier("elephant")
    private Animal Animal1;

    @Autowired  // 验证首选装配@Primary
    private Animal Animal2;

    @Override
    public String toString() {
        return "AnimalDao{" + "cat=" + cat + ", animal=" + animal +
                ", Animal1=" + Animal1 + ", Animal2=" + Animal2 + '}';
    }
}
