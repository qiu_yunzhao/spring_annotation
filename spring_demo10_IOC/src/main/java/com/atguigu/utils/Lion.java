package com.atguigu.utils;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Primary // 首选装配组件
@Component
public class Lion implements Animal {
}
