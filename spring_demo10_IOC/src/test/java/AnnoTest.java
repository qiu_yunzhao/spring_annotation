import com.atguigu.utils.Cat;
import com.atguigu.utils.Dog;
import com.atguigu.utils.Lion;
import com.atguigu.config.SpringConfig;
import com.atguigu.dao.AnimalDao;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AnnoTest {
    @Test
    public void test1() {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringConfig.class);
//        /**
//         * 获取容器中注册实体类的名称
//         */
//        String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
//        for (String name : beanDefinitionNames) {
//            System.out.println(name);
//        }
        AnimalDao animalDao = applicationContext.getBean("animalDao", AnimalDao.class);
        System.out.println("animalDao-->" + animalDao);
        Cat cat = applicationContext.getBean("cat", Cat.class);
        System.out.println("cat-->" + cat);
        Dog dog = applicationContext.getBean("dog", Dog.class);
        System.out.println("dog-->" + dog);
        Lion lion = applicationContext.getBean("lion", Lion.class);
        System.out.println("lion-->" + lion);
    }
}
