import com.atguigu.config.SpringConfig;
import com.atguigu.service.MathCalculator;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AnnoTest {
    @Test
    public void test1() {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringConfig.class);

        MathCalculator calculator = applicationContext.getBean("calculator", MathCalculator.class);
        int res = calculator.div(10, 2);
        System.out.println("计算结果：" + res);

    }
}