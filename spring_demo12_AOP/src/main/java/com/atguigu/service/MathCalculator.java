package com.atguigu.service;

import org.springframework.stereotype.Component;

@Component("calculator")
public class MathCalculator {

    public int div(int i, int j) {
        System.out.println("MathCalculator.div()执行--参数：i=" + i + "j=" + j);
        return i / j;
    }

}
