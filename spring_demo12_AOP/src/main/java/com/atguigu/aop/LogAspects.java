package com.atguigu.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.lang.reflect.Array;
import java.util.Arrays;

//日志切面类(增强类)
@Component
@Aspect //表示生成代理对象
public class LogAspects {
    /**
     * 抽取公共切入点表达式函数
     */
    @Pointcut(value = "execution(* com.atguigu.service.MathCalculator.div(..))")
    public void commonPointcut() {
    }

    /**
     * 前置通知
     */
    @Before(value = "execution(* com.atguigu.service.MathCalculator.div(..))")
    public void logBefore(JoinPoint joinPoint) {
        String functionName = joinPoint.getSignature().getName(); // 获取方法名
        Object[] args = joinPoint.getArgs(); // 参数表
        System.out.println("@Before前置通知 -- 方法：" + functionName + "--参数列表" + Arrays.asList(args));
    }

    /**
     * 异常通知
     * value      切入点
     * throwing   指定异常由哪个参数接收
     */
    @AfterThrowing(value = "execution(public int com.atguigu.service.MathCalculator.div(int, int))", throwing = "exception")
    public void logAfterThrowing(Exception exception) {
        System.out.println("@AfterThrowing异常通知 -- 异常信息：" + exception);
    }

    /**
     * 环绕通知
     */
    @Around(value = "execution(int com.atguigu.service.MathCalculator.*(int, int))")
    public Object logAround(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        System.out.println("@Around环绕通知之前");
        Object res = proceedingJoinPoint.proceed(); //被增强的方法执行
        System.out.println("@Around环绕通知之后");
        return res;
    }

    /**
     * 最终通知
     */
    @After(value = "commonPointcut()")
    public void logAfter() {
        System.out.println("@After最终通知");
    }

    /**
     * 后置通知（返回通知）
     * value       切入点
     * returning   指定返回值由哪个参数接收
     */
    @AfterReturning(value = "com.atguigu.aop.LogAspects.commonPointcut()", returning = "result")
    public void logAfterReturning(JoinPoint joinPoint, Object result) {
        System.out.println("@AfterReturning后置通知 -- 返回值：" + result);
    }

}
