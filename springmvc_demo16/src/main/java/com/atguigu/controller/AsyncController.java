package com.atguigu.controller;

import com.atguigu.service.DeferredResultQueue;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.UUID;
import java.util.concurrent.Callable;

@Controller
public class AsyncController {

    @ResponseBody
    @RequestMapping("/async1")
    public Callable<String> async1() {
        System.out.println("主线程开始-->" + Thread.currentThread() + "--" + System.currentTimeMillis());

        Callable<String> callable = new Callable<String>() {
            @Override
            public String call() throws Exception {
                System.out.println("子线程开始-->" + Thread.currentThread() + "--" + System.currentTimeMillis());
                Thread.sleep(3000);
                System.out.println("子线程结束-->" + Thread.currentThread() + "--" + System.currentTimeMillis());
                return "AsyncController.async1()";
            }
        };

        System.out.println("主线程结束-->" + Thread.currentThread() + "--" + System.currentTimeMillis());
        return callable;
    }


    /**
     * 模拟接受请求的 应用1
     */
    @RequestMapping(value = "/createOrder", produces = "application/json; charset=utf-8")
    @ResponseBody
    public DeferredResult<Object> createOrder() {
        // 设置超时时间，超过时间给出错误提示"订单创建失败"
        DeferredResult<Object> deferredResult = new DeferredResult<Object>((long) 5000, "超时创建订单失败");
        // 将deferredResult存到某个地方
        DeferredResultQueue.save(deferredResult);
        return deferredResult;
    }

    /**
     * 模拟实际处理请求请求的 应用2
     */
    @RequestMapping("/handelOrder")
    @ResponseBody
    public String handelOrder() {
        String orderId = UUID.randomUUID().toString();
        // 将deferredResult从存储的地方取出后设置处理结果
        DeferredResultQueue.get().setResult(orderId);
        return "success-->" + orderId;
    }

}
