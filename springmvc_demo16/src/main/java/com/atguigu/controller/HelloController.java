package com.atguigu.controller;

import com.atguigu.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloController {

    @Autowired
    HelloService helloService;

    @ResponseBody
    @RequestMapping("/hello")
    public String hello() {
        String toast = helloService.sayHello("tomcat9");
        return toast;
    }

    // 视图解析器解析路径为：web\WEB-INF\success.jsp
    // 自定义视图解析器解析路径为：web\WEB-INF\views\success.jsp
    @RequestMapping("/success")
    public String success(){
        return "success";
    }

}
