package com.atguigu;

import com.atguigu.config.AppConfig;
import com.atguigu.config.RootConfig;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.ServletContainerInitializer;

//该类在web容器启动时创建对象，会调用其中的方法来初始化容器以及前端控制(映射)器
public class MyWebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    /**
     * 获取根容器的配置类（Spring的配置文件），用来生成父容器
     */
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{RootConfig.class};
    }

    /**
     * 获取web容器的配置类（SpringMVC的配置文件），用来生成子容器
     */
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{AppConfig.class};
    }

    /**
     * 获取 DispatcherServlet 的映射信息
     * <p>
     * "/"  拦截所有请求，包括 x.js,xx.xpng 等静态资源，不包括 xx.jsp
     * "/*" 拦截所有请求，包括 x.js,xx.xpng 等静态资源，包括 xx.jsp
     */
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }
}
