package com.atguigu.service;

import org.springframework.web.context.request.async.DeferredResult;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * 用于保存异步请求DeferredResult的类
 */
public class DeferredResultQueue {

    private static Queue<DeferredResult<Object>> deferredResultQueue = new ConcurrentLinkedQueue<>();

    public static void save(DeferredResult<Object> deferredResult) {
        deferredResultQueue.add(deferredResult);
    }

    public static DeferredResult<Object> get() {
        return deferredResultQueue.poll();
    }
}
