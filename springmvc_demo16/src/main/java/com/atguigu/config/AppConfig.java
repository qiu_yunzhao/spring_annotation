package com.atguigu.config;

import com.atguigu.interceptors.MyInterceptors;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

/**
 * 为了形成父子容器，SpringMVC包扫描时只扫描 @Controller 注解
 */
@ComponentScan(value = "com.atguigu", includeFilters = {
        @ComponentScan.Filter(type = FilterType.ANNOTATION, classes = {Controller.class})
}, useDefaultFilters = false)
@EnableWebMvc // 开启SpringMVC自定义配置功能
public class AppConfig implements WebMvcConfigurer {

    /**
     * 自定义 视图解析器
     */
    @Override
    public void configureViewResolvers(ViewResolverRegistry registry) {
        /**
         *  默认从 "/WEB-INF/" 路径开始找 "*.jsp"
         *  配置为 "/WEB-INF/views/" 路径开始找 "*.jsp"
         */
        registry.jsp("/WEB-INF/views/", ".jsp");
    }

    /**
     * 自定义 静态资源访问
     * 静态资源默认交给sprigMVC匹配（会找不到资源），配置后交由tomcat匹配
     */
    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    /**
     * 自定义 拦截器
     * 静态资源默认交给sprigMVC匹配（会找不到资源），配置后交由tomcat匹配
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new MyInterceptors()) // 添加自定义拦截器类
        .addPathPatterns("/**"); // 拦截任意多层路径
    }
}
