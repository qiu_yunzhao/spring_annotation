package com.atguigu.utils;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@Data
// 读取配置文件，将其中k/v保存到运行环境变量
@PropertySource(value = {"classpath:/person.properties"})
public class Person {
    /**
     * @Value 可取值有：
     * 1 基本数据类型
     * 2 SpEL表达式： #{}
     * 3 取配置文件（运行环境变量里的值）的值：${}
     */

    @Value("张三")
    private String name;
    @Value("#{20 - 2}")
    private Integer age;
    @Value("${person.address}")
    private String address;
}
