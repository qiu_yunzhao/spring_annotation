
import com.atguigu.utils.Person;
import com.atguigu.config.SpringConfig;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;


public class AnnoTest {
    @Test
    public void test() {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringConfig.class);

        // 获取配置文件加载到运行环境中的变量
        ConfigurableEnvironment environment = applicationContext.getEnvironment();
        String property = environment.getProperty("person.address");
        System.out.println("person.address -- >" +property);

        Person person = applicationContext.getBean("person", Person.class);
        System.out.println(person);
    }

}
