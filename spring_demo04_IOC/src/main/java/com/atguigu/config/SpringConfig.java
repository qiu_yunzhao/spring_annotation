package com.atguigu.config;

import com.atguigu.utils.Person;
import com.atguigu.condition.LinuxCondition;
import com.atguigu.condition.WindowsCondition;
import org.springframework.context.annotation.*;

@Configuration
@ComponentScan("com.atguigu.utils")
@Conditional({WindowsCondition.class})
public class SpringConfig {

    @Conditional({WindowsCondition.class})
    @Bean("bill")
    public Person person1() {
        return new Person("Bill Gates", 62);
    }

    @Conditional({LinuxCondition.class})
    @Bean("linus")
    public Person person2() {
        return new Person("Linus", 48);
    }

}
