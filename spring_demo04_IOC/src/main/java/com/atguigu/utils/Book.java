package com.atguigu.utils;

import com.atguigu.condition.LinuxCondition;
import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Component;

@Component
@Conditional({LinuxCondition.class})
public class Book {
}
