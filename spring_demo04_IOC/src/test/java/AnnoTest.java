import com.atguigu.utils.Book;
import com.atguigu.utils.Person;
import com.atguigu.config.SpringConfig;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.Environment;

import java.util.Map;

public class AnnoTest {
    @Test
    public void test() {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringConfig.class);

        // 获取环境信息(环境变量、虚拟机变量等)
        Environment environment = applicationContext.getEnvironment();
        String property = environment.getProperty("os.name");
        System.out.println("当前环境：" + property);

        // 获取IOC容器中符合类型的所有bean的名称
        String[] beanNamesForType = applicationContext.getBeanNamesForType(Book.class);
        for (String name : beanNamesForType) {
            System.out.println(name);
        }
        // 获取IOC容器中符合类型的所有bean实例
        Map<String, Person> beansOfType = applicationContext.getBeansOfType(Person.class);
        System.out.println(beansOfType);
    }

}
