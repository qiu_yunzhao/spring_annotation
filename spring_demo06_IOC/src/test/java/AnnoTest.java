import com.atguigu.config.SpringConfig;
import com.atguigu.controller.BookController;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AnnoTest {

    @Test
    public void test() {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringConfig.class);

        //Person person1 = applicationContext.getBean("person", Person.class);
        //System.out.println(person1);
        BookController bookController1 = applicationContext.getBean("bookController", BookController.class);
        System.out.println(bookController1);

        System.out.println("容器启动完成");
    }

}
