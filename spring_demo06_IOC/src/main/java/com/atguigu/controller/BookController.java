package com.atguigu.controller;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

@Controller
@Lazy
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class BookController {

    public BookController() {
        System.out.println("BookController实例被创建");
    }
}
