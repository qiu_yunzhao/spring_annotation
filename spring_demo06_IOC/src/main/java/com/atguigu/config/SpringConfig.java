package com.atguigu.config;

import com.atguigu.utils.Person;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;

@ComponentScan("com.atguigu.controller")
public class SpringConfig {

    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    @Lazy
    @Bean("person")
    public Person person() {
        System.out.println("Person实例被创建");
        return new Person();
    }
}