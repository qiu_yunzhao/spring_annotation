
import com.atguigu.config.SpringConfig;
import org.junit.Test;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;


public class AnnoTest {
    @Test
    public void test() {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringConfig.class);
        // 定义事件(ApplicationEvent的参数可以是任何对象)
        ApplicationEvent applicationEvent =
                new ApplicationEvent(new String("自己发布的事件")) {
                };
        //发布事件
        applicationContext.publishEvent(applicationEvent);
        //关闭容器
        applicationContext.close();
    }

}
