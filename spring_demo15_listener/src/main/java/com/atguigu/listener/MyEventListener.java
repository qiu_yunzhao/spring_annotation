package com.atguigu.listener;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class MyEventListener {


    @EventListener(classes = {ApplicationEvent.class})
    public void listenEvent(ApplicationEvent event) {
        System.out.println("MyEventListener.listenEvent()收到事件-->"+event);
    }

}
