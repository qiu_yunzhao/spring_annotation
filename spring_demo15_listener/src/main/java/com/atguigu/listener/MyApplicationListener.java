package com.atguigu.listener;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * ApplicationListener接口监听容器中发布的事件，用于事件驱动模型开发；
 *
 * onApplicationEvent（）方法，当容器中有事件发布后会触发。
 */
@Component
public class MyApplicationListener implements ApplicationListener<ApplicationEvent> {

    /**
     *  参数：ApplicationEvent 监听事件的父类
     */
    public void onApplicationEvent(ApplicationEvent event) {
        System.out.println("MyApplicationListener收到事件-->"+event);
    }

}
