package com.atguigu.processor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

/**
 * BeanPostProcessor接口是bean后置处理器，在IOC容器每个bean初始化前后进行一些处理工作。
 * bean初始化在调用bean构造方法执行后执行
 */
@Component
public class MyBeanPostProcessor implements BeanPostProcessor {

    //bean初始化之前执行
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("MyBeanPostProcessor.postProcessBeforeInitialization()--" + beanName);
        // 这里可以对当前bean进行扩展
        return bean;
    }

    //bean初始化之后执行
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("MyBeanPostProcessor.postProcessAfterInitialization()--" + beanName);
        // 这里可以对当前bean进行扩展
        return bean;
    }

}
