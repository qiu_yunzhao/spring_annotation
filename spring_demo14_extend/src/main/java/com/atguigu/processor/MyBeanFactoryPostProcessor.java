package com.atguigu.processor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.stereotype.Component;

/**
 * BeanFactoryPostProcessor 接口是BeanFactory后置处理器，用来定制和修改BeanFactory
 * BeanFactory标准初始化指所有bean的定义已经保存加载到BeanFactory,但是bean的实例还没创建的时刻。
 */
@Component
public class MyBeanFactoryPostProcessor implements BeanFactoryPostProcessor {

    //在BeanFactory标准初始化之后调用
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        System.out.println("MyBeanFactoryPostProcessor.postProcessBeanFactory()--当前定义的bean数量："
                + beanFactory.getBeanDefinitionCount());
        // 在这里可以修改和扩展BeanFactory
    }

}
