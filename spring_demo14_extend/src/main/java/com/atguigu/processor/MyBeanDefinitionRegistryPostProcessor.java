package com.atguigu.processor;

import com.atguigu.bean.Red;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.*;
import org.springframework.stereotype.Component;

/**
 * BeanDefinitionRegistryPostProcessor 接口是BeanFactoryPostProcessor接口的子接口，是BeanFactory后置处理器。用来定制和修改BeanFactory
 * BeanDefinitionRegistryPostProcessor先于BeanFactoryPostProcessor执行。
 *
 * postProcessBeanDefinitionRegistry()方法在所有bean的定义信息将要加载到beanFactory,但是bean的实例还没创建的时刻。
 * postProcessBeanFactory()方法在所有bean的定义信息已经加载到beanFactory,但是bean的实例还没创建的时刻，postProcessBeanDefinitionRegistry()之后。
 */
@Component
public class MyBeanDefinitionRegistryPostProcessor implements BeanDefinitionRegistryPostProcessor {

    /**
     * 参数BeanDefinitionRegistry是IOC容器定义信息的保存中心，
     * BeanFactory就是根据BeanDefinitionRegistry中保存的每个bean定义信息来创建bean实例
     */
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        System.out.println("MyBeanDefinitionRegistryPostProcessor.postProcessBeanDefinitionRegistry()--当前定义的bean数量："
                + registry.getBeanDefinitionCount());
        // 可以在这里注册bean
        BeanDefinition beanDefinition1 = new RootBeanDefinition(Red.class); // 方式1
        // BeanDefinition beanDefinition2 = BeanDefinitionBuilder.rootBeanDefinition(Red.class).getBeanDefinition();//方式2
        registry.registerBeanDefinition("red1", beanDefinition1);
    }

    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        System.out.println("MyBeanDefinitionRegistryPostProcessor.postProcessBeanFactory()--当前定义的bean数量："
                + beanFactory.getBeanDefinitionCount());
        // 在这里可以修改和扩展BeanFactory
    }
}
