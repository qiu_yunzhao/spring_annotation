package com.atguigu.bean;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class Red {
    public Red() {
        System.out.println("Red构造方法被执行");
    }

    // 对象调用构造方法构建完成，并赋值好之后调用
    @PostConstruct
    public void init() {
        System.out.println("Red.init()被调用");
    }

    // 容器关闭的时候调用
    @PreDestroy
    public void destroy() {
        System.out.println("Red.destroy()被调用");
    }
}
