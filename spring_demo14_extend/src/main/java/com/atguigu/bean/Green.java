package com.atguigu.bean;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class Green {
    public Green() {
        System.out.println("Green构造方法被执行");
    }

    // 对象调用构造方法构建完成，并赋值好之后调用
    @PostConstruct
    public void init() {
        System.out.println("Green.init()被调用");
    }

    // 容器关闭的时候调用
    @PreDestroy
    public void destroy() {
        System.out.println("Green.destroy()被调用");
    }
}
