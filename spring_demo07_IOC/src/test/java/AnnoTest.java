
import com.atguigu.config.SpringConfig;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class AnnoTest {
    @Test
    public void test() {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringConfig.class);

        String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
        for (String name : beanDefinitionNames) {
            System.out.println(name);
        }

        // 工厂bean获取的是调用 getObject() 方法返回的对象，而不是其本身
        Object blue1 = applicationContext.getBean("colorFactoryBean");
        System.out.println(blue1);
        Object blue2 = applicationContext.getBean("colorFactoryBean");
        System.out.println(blue2);

        // 获取colorFactoryBean本身
        Object colorFactoryBean = applicationContext.getBean("&colorFactoryBean");
        System.out.println(colorFactoryBean);
    }

}
