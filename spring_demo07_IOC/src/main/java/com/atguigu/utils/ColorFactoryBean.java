package com.atguigu.utils;

import org.springframework.beans.factory.FactoryBean;

public class ColorFactoryBean implements FactoryBean {
    //返回一个Blue的对象，该对象会被添加到容器中
    public Object getObject() throws Exception {
        System.out.println("ColorFactoryBean.getObject()被调用来获取Blue对象");
        return new Blue();
    }

    //返回对象的类型是Blue
    public Class<?> getObjectType() {
        return Blue.class;
    }

    //返回单例对象
    public boolean isSingleton() {
        return false;
    }
}
