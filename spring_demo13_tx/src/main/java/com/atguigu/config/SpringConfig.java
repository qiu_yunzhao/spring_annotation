package com.atguigu.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@Configuration
@ComponentScan("com.atguigu")
@PropertySource("classpath:/jdbc.properties") // 引入配置文件数据到环境变量
@EnableTransactionManagement // 开启基于注解的事务管理
public class SpringConfig {

    // 获取配置文件数据
    @Value("${mysql.userName}")
    private String user;
    @Value("${mysql.password}")
    private String password;
    @Value("${mysql.url}")
    private String url;
    @Value("${mysql.driverClass}")
    private String driverClass;

    // 注册数据库连接池
    @Bean
    public DataSource dataSource() {
        DruidDataSource druidDataSource = new DruidDataSource();
        druidDataSource.setUsername(user);
        druidDataSource.setPassword(password);
        druidDataSource.setUrl(url);
        druidDataSource.setDriverClassName(driverClass);
        return druidDataSource;
    }

    // 注册JdbcTemplate(使用的数据源从容器中获取)
    @Bean
    public JdbcTemplate jdbcTemplate() {
        // 配置类中调用注册组件的方法dataSource()不会创建新对象，而是从容器中获取;
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource());
        return jdbcTemplate;
    }

    //注册事务管理器
    @Bean
    // 配置类中注册组件的方法参数dataSource默认从容器中获取
    public PlatformTransactionManager transactionManager(DataSource dataSource) {
        PlatformTransactionManager transactionManager = new DataSourceTransactionManager(dataSource);
        return transactionManager;
    }
}
