package com.atguigu.service;

import com.atguigu.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional // 告诉spring该类的所有方法支持事务
public class UserService {

    @Autowired
    private UserDao userDao;

    @Transactional // 告诉spring该方法支持事务
    public void insertUser() {
        userDao.insert();
        System.out.println("已执行insertUser插入操作");
        int i = 10 / 0;
    }
}
