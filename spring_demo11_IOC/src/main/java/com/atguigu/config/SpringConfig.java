package com.atguigu.config;

import com.atguigu.utils.Dog;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.EmbeddedValueResolverAware;
import org.springframework.context.annotation.*;
import org.springframework.util.StringValueResolver;

import javax.sql.DataSource;

@Profile({"test","dev","prod"})
@Configuration
@PropertySource("classpath:/dbconfig.properties")
public class SpringConfig implements EmbeddedValueResolverAware {

    // 第一种方式获取配置文件数据
    @Value("${db.user}")
    private String user;
    @Value("${db.password}")
    private String password;
    @Value("${db.driverClass}")
    private String driverClass;

    // 第2种方式获取配置文件数据
    private StringValueResolver resolver;

    public void setEmbeddedValueResolver(StringValueResolver resolver) {
        this.resolver = resolver;
    }

    @Bean()
    public Dog dog() {
        return new Dog();
    }

    @Profile("test")
    @Bean()
    public DataSource dataSourceTest() throws Exception {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        dataSource.setUser(user);
        dataSource.setPassword(password);
        dataSource.setJdbcUrl("jdbc:mysql://localhost:3306/test_db");
        dataSource.setDriverClass(driverClass);
        return null;
    }

    @Profile("prod")
    @Bean
    // 第3种方式获取配置文件数据
    public DataSource dataSourceProd(@Value("${db.driverClass}") String driverClass) throws Exception {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        dataSource.setUser(user);
        dataSource.setPassword(password);
        dataSource.setJdbcUrl("jdbc:mysql://localhost:3306/prod_db");
        dataSource.setDriverClass(driverClass);
        return null;
    }

    @Profile("dev")
    @Bean
    public DataSource dataSourceDev() throws Exception {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        dataSource.setUser(user);
        dataSource.setPassword(password);
        dataSource.setJdbcUrl("jdbc:mysql://localhost:3306/dev_db");
        String driverClass = resolver.resolveStringValue("${db.driverClass}");
        dataSource.setDriverClass(driverClass);
        return null;
    }
}
