package com.atguigu.selector;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

//自定义逻辑返回需要导入的组件的类
public class MyImportSelector implements ImportSelector {

    //方法返回值就是要导入到IOC容器中的组件
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        return new String[]{"com.atguigu.bean.Blue", "com.atguigu.bean.Red"};
    }

}