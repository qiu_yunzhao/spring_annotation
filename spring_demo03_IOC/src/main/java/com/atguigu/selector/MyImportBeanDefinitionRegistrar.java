package com.atguigu.selector;

import com.atguigu.utils.Cat;
import com.atguigu.utils.Dog;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

public class MyImportBeanDefinitionRegistrar implements ImportBeanDefinitionRegistrar {

    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        // 查看是否包含某些bean
        boolean redDefinitioned = registry.containsBeanDefinition("com.atguigu.bean.Blue");
        boolean blueDefinitioned = registry.containsBeanDefinition("com.atguigu.bean.Red");

        if (redDefinitioned && blueDefinitioned) {
            // 如果已经注册了某些bean就往容器中注册Dog的实例，并指定beanName为dog_luck
            BeanDefinition beanDefinition = new RootBeanDefinition(Dog.class);
            registry.registerBeanDefinition("dog_luck", beanDefinition);
        } else {
            // 否则就往容器中注册Cat的实例，并指定beanName为cat_tom
            BeanDefinition beanDefinition = new RootBeanDefinition(Cat.class);
            registry.registerBeanDefinition("cat_tom", beanDefinition);
        }
    }

}
