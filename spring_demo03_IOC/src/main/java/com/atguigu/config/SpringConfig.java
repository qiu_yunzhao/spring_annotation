package com.atguigu.config;

import com.atguigu.utils.Person;
import com.atguigu.selector.MyImportBeanDefinitionRegistrar;
import com.atguigu.selector.MyImportSelector;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({Person.class, MyImportSelector.class, MyImportBeanDefinitionRegistrar.class}) // 快速注册，可以导入多个
public class SpringConfig {
}