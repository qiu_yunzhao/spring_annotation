package com.atguigu.config;

import com.atguigu.utils.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Configuration 告诉spring这是一个配置类
 * class:类型为返回值类型
 * id:默认是方法名作为id
 */
@Configuration
public class SpringConfig {

    @Bean
    public Person person() {
        return new Person("李四", 20);
    }

}