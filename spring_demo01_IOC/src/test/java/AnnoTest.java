import com.atguigu.utils.Person;
import com.atguigu.config.SpringConfig;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AnnoTest {
    @Test
    public void test1() {
        /**
         * 通过AnnotationConfigApplicationContext加载配置类创建容器
         */
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringConfig.class);
        Person person = applicationContext.getBean("person", Person.class);
        System.out.println(person);
    }
}