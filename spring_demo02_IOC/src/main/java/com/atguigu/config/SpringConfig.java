package com.atguigu.config;

import com.atguigu.service.BookService;
import org.springframework.context.annotation.*;
import org.springframework.stereotype.Controller;

/**
 * @Configuration 告诉spring这是一个配置类
 * class:类型为返回值类型
 * id:默认是方法名作为id
 */
@Configuration

///**
// * @ComponentScan 包扫描注解
// *  value：指定要扫描的包
// *  excludeFilters: 扫描时排除哪些
// */
//@ComponentScan(value = "com.atguigu", excludeFilters = {
//        /**
//         * excludeFilters是一个Filter类型的数组
//         *   type：过滤规则
//         */
//        @ComponentScan.Filter(type = FilterType.ANNOTATION, classes = {Controller.class}),
//})


/**
 * @ComponentScan 包扫描注解
 *  value：指定要扫描的包
 *  includeFilters: 扫描时只扫描哪些
 *  useDefaultFilters:禁用默认扫描规则，否则includeFilters不生效
 */
@ComponentScan(value = "com.atguigu", includeFilters = {
        /**
         * includeFilters是一个Filter类型的数组
         *   type：过滤规则
         */
        @ComponentScan.Filter(type = FilterType.ANNOTATION, classes = {Controller.class}),
        @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {BookService.class}),
        @ComponentScan.Filter(type = FilterType.CUSTOM, classes = {MyTypeFilter.class})
}, useDefaultFilters = false)

//@ComponentScans(value = {
//        @ComponentScan(value = "com.atguigu", excludeFilters = {
//                @ComponentScan.Filter(type = FilterType.ANNOTATION, classes = {Controller.class})
//        }),
//        @ComponentScan(value = "com.atguigu", includeFilters = {
//                @ComponentScan.Filter(type = FilterType.ANNOTATION, classes = {Controller.class})
//        }, useDefaultFilters = false)
//})
public class SpringConfig {

//    /**
//     * @Bean 给容器注册bean
//     * class:类型为函数返回值的类型
//     * id:默认是用方法名作为id,可以使用value属性为其指定id
//     */
//    @Bean(value = "person1")
//    public Person person() {
//        return new Person("李四", 20);
//    }

}
