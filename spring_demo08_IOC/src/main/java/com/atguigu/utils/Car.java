package com.atguigu.utils;

public class Car {

    public Car() {
        System.out.println("Car 构造方法被调用");
    }

    // 对象调用构造方法构建完成，并赋值好之后调用
    public void init() {
        System.out.println("Car.init()被调用");
    }

    // 容器关闭的时候调用
    public void destroy() {
        System.out.println("Car.destroy()被调用");
    }
}
