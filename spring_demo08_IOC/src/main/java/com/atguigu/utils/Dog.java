package com.atguigu.utils;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class Dog {

    public Dog() {
        System.out.println("Dog 构造方法被调用");
    }

    // 对象调用构造方法构建完成，并赋值好之后调用
    @PostConstruct
    public void init() {
        System.out.println("Dog.@PostConstruct被调用");
    }

    // 容器移除对象之前
    @PreDestroy
    public void destroy() {
        System.out.println("Dog.@PreDestroy被调用");
    }

}
