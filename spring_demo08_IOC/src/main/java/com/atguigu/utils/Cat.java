package com.atguigu.utils;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class Cat implements InitializingBean, DisposableBean {

    public Cat() {
        System.out.println("Cat 构造方法被调用");
    }

    // 对象调用构造方法构建完成，并赋值好之后调用
    public void afterPropertiesSet() throws Exception {
        System.out.println("Cat.afterPropertiesSet()被调用");
    }

    // 容器关闭的时候调用
    public void destroy() {
        System.out.println("Cat.destroy()被调用");
    }

}
