package com.atguigu.config;

import com.atguigu.utils.Car;
import com.atguigu.utils.Cat;
import com.atguigu.utils.Dog;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
@ComponentScan("com.atguigu")
public class SpringConfig {

    @Bean(initMethod = "init", destroyMethod = "destroy")
    @Scope("singleton")
    public Car car() {
        return new Car();
    }

    @Scope("singleton")
    @Bean
    public Cat cat() {
        return new Cat();
    }

    @Scope("singleton")
    @Bean
    public Dog dog() {
        return new Dog();
    }

}